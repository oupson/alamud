# Consignes pour jouer :
## Commandes :

### De déplacement :

```
aller 'direction'
haut
bas
---
Où 'direction' prend l un des quatre point cardinaux, écrit entièrement ou abrégé.
Ex : aller no -> (aller nord ouest)
```
Si la réponse à cette commande est "il n'a pas d'issue dans cette direction." Vous ne pourrez jamais y accéder. 
Si le message n'est pas celui-là, alors vous pourrez accéder à cet endroit après avoir parlé à quelqu'un ou ramassé un objet.

### D'intéractions avec les lieux :

```
regarder
regarder 'lieu'
---
Où 'lieu' n est précisément écrit mais généralement insinué "..."
```

### D'intéractions avec les objets : 

```
regarder 'objet'
prendre 'objet'
---
Où 'objet' est le nom de l objet, de la carte.
```

### D'intéractions avec les personnages :

```
regarder 'personne'
parler 'personne' // parler a 'personne'
demander a 'personne' de parler de 'sujet'
montrer 'objet' a 'personne'
---
Où 'personne' est le nom de la personne et 'sujet' est le sujet sur lequel vous voulez interroger le protagonniste. 
```

```
IMPORTANT : les noms de variables, 'lieu', 'objet', 'personne', 'sujet' sont généralement des mots uniques ou des petits groupes nominaux. 
```


## Commande pour terminer le jeu

```
Je pense que 'personne' est le tueur
```

---

Il est fortement conseillé de prendre des notes à côté pour plus de simplicité, si vous voulez répondre à l'énigme. 

Bon jeu à vous !