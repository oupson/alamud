# INSTALLATION
## Linux :

Create a virtualenv:

```bash
virtualenv -p python3 venv
source venv/bin/activate
pip install tornado
pip install pyyaml
pip install Pillow
```

## Windows :

Create a virtualenv:

```cmd
virtualenv -p python3 venv
cd venv/Scripts
activate.bat
cd ../..
pip install tornado
pip install pyyaml
pip install Pillow
```

# RUNNING

## Linux :
  ```bash
  ./mud.py
  ```

## Windows :
```cmd
"venv/Scripts/python" mud.py
```

# USING
Direct your browser to http://localhost:9999/

The server actually listens on all interfaces.

# COMMANDES
SEE [COMMANDES.md](COMMANDES.MD)

# [Diaporama de Présentation](https://docs.google.com/presentation/d/e/2PACX-1vSUiBDHwL8sOi8Ue1GLHri2h0BVttK2KB372l7X2_wgnf2Zh8-6aL7KxuBBUqlhviErv2s_042WEmVi/pub?start=false&loop=false&delayms=3000)