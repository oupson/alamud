from .event import Event3
from mud.effects.effect import Effect

class ShowEvent(Event3):
    NAME = "show"
    def perform(self):
        if not self.object2.has_prop("showable"):
            self.add_prop("object-not-showable")
            return self.question_failed()
        if self.can_see(self.object):
            if self.object.id in self.object2.showables:
                self.inform("show")
                effects = self.object2.showables[self.object.id].get("effects")
                if effects != None:
                    for effect in effects:
                        Effect.make_effect(effect, self.world_context()).execute()
            else:
                self.inform("show.failed")
        else:
            self.question_failed(self, "show.cant_see")
    
    def can_see(self, thing):
        for x in self.object2.container().contents():
            if x is self.actor:
                pass
            if x.id == thing.id:
                return True
        for x in self.actor.contents():
            if x.id == thing.id:
                return True
        return False

    def question_failed(self, id = "show.failed"):
        self.fail()
        self.inform(id)
