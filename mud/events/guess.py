from .event import Event2
from mud.effects import DeathEffect

class GuessEvent(Event2):
    NAME = "guess"

    def perform(self):
        if not self.object.is_killer:
            return self.take_failed()
        self.inform("guess")

    def take_failed(self):
        self.inform("guess.failed")
        DeathEffect({}, self.context()).execute()
        
        self.execute_effects()