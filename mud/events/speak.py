from .event import Event2

class SpeakEvent(Event2):
    NAME = "speak"
    def perform(self):
        if not self.object.has_prop("speakable"):
            self.add_prop("object-not-speakable")
            return self.take_failed()
        self.inform("speak")

    def take_failed(self):
        self.fail()
        self.inform("speak.failed")
