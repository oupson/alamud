# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2
from mud.models import Personn

class InspectEvent(Event2):
    NAME = "look"

    def get_event_templates(self):
        return self.object.get_event_templates()

    def perform(self):
        if not self.actor.can_see():
            return self.failed_cannot_see()
        self.buffer_clear()
        self.buffer_inform("look.actor", object=self.object)
        if not (self.object.is_container() or isinstance(self.object, Personn)) or self.object.has_prop("closed"):
            return self.actor.send_result(self.buffer_get())
        if self.object.is_container():
            players = []
            objects = []
            for x in self.object.contents():
                if x is self.actor:
                    pass
                elif x.is_player():
                    players.append(x)
                else:
                    objects.append(x)

            if players or objects:
                self.buffer_inform("look.inside.intro")
                self.buffer_append("<ul>")
                for x in players:
                    self.buffer_peek(x)
                for x in objects:
                    self.buffer_peek(x)
                self.buffer_append("</ul>")
            else:
                self.buffer_inform("look.inside.empty" )
        elif isinstance(self.object, Personn):
            questions = []
            for question in self.object.questions:
                prop = question.get("prop")
                if prop != None:
                    if prop in self.object.get_props():
                        questions.append(
                            question["names"][0]
                        )
                else:
                    questions.append(
                        question["names"][0]
                    )
            if questions:
                self.buffer_inform("look.questions-intro")
                self.buffer_append("<ul>")
                for q in questions:
                    self.buffer_append("<li>%s</li>" % q)
                self.buffer_append("</ul>")
        self.actor.send_result(self.buffer_get())

    def failed_cannot_see(self):
        self.fail()
        self.buffer_clear()
        self.buffer_inform("look.failed")
        self.actor.send_result(self.buffer_get())
