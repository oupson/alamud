from .event import Event3
from mud.effects.effect import Effect

class QuestionEvent(Event3):
    NAME = "question"
    def perform(self):
        if not self.object.has_prop("questionnable"):
            self.add_prop("object-not-questionnable")
            return self.question_failed()
        self.inform("question")
        effects = self.object2.get("effects")
        if effects != None:
            for effect in effects:
                Effect.make_effect(effect, self.world_context()).execute()
                

    def question_failed(self):
        self.fail()
        self.inform("question.failed")
