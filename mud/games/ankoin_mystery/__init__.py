# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from mud.actions import (
    GoAction, TakeAction, LookAction, InspectAction, OpenAction,
    OpenWithAction, CloseAction, TypeAction, InventoryAction,
    LightOnAction, LightOffAction, DropAction, DropInAction,
    PushAction, TeleportAction, EnterAction, LeaveAction,
    SpeakAction, QuestionAction, ShowAction, GuessAction
)

import mud.game

def make_rules():
    GAME = mud.game.GAME
    DIRS = list(GAME.static["directions"]["noun_at_the"].values())
    DIRS.extend(GAME.static["directions"]["noun_the"].values())
    DIRS.extend(GAME.static["directions"]["normalized"].keys())
    DETS = "(?:l |le |la |les |une |un |son |sa |ses |ces |ton |du |)"

    return (
        (GoAction       , r"(?:aller |)(%s)" % "|".join(DIRS)),
        (TakeAction     , r"(?:p|prendre) %s(.+)" % DETS),
        (LookAction     , r"(?:r|regarder)"),
        (InspectAction  , r"(?:r|regarder|lire|inspecter|observer) %s(.+)" % DETS),
        (OpenAction     , r"ouvrir %s(.+)" % DETS),
        (OpenWithAction , r"ouvrir %s(.+) avec %s(\w+)" % (DETS,DETS)),
        (CloseAction    , r"fermer %s(.+)" % DETS),
        (TypeAction     , r"(?:taper|[eé]crire) (.+)$"),
        (InventoryAction, r"(?:inventaire|inv|i)$"),
        (LightOnAction  , r"allumer %s(.+)" % DETS),
        (LightOffAction , r"[eé]teindre %s(.+)" % DETS),
        (DropAction     , r"(?:poser|laisser) %s(.+)" % DETS),
        (DropInAction   , r"(?:poser|laisser) %s(.+) (?:dans |sur |)%s(.+)" % (DETS,DETS)),
        (PushAction     , r"(?:appuyer|pousser|presser)(?: sur|) %s(.+)" % DETS),
        (TeleportAction , r"tele(?:porter|) (\S+)"),
        (EnterAction    , r"entrer"),
        (LeaveAction    , r"sortir|partir"),
        (SpeakAction    , r"parler (?:a |à |au |)(.+)"),
        (QuestionAction , r"demander (?:a|à|au) (.+) de parler de %s(.+)" % DETS),
        (QuestionAction , r"(.*?),? parle moi de %s(.+)" % DETS),
        (ShowAction     , r"(?:montrer|parler de|pr[ée]senter) %s(.+) (?:a|à|au) (.+)" % (DETS)),
        (GuessAction    , r"je pense que %s(.+) est le tueur" % (DETS))
    )
