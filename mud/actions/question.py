from .action import Action3
from mud.events import QuestionEvent
from mud.models import Personn

class QuestionAction(Action3):
    EVENT = QuestionEvent
    RESOLVE_OBJECT = "resolve_for_take"
    ACTION = "question"

    def resolve_object2(self):
        personn = self.resolve_object()
        if isinstance(personn,Personn):
            for question in personn.questions:
                if self.object2 in question["names"]:
                    prop = question.get("prop")
                    if prop != None:
                        if prop in personn.get_props():
                            return question
                        else:
                            return None
                    else:
                        return question
        else:
            return "error" # Pour avoir le message d'erreur en vain
        return None