from .action import Action3
from mud.events import ShowEvent
from mud.models import Personn

class ShowAction(Action3):
    EVENT = ShowEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_take"
    ACTION = "show"