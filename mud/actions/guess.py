from .action import Action2
from mud.events import GuessEvent
from mud.models import Player, Personn
from mud.game import GAME

class GuessAction(Action2):
    EVENT = GuessEvent
    ACTION = "guess"

    def resolve_object(self): # Find the matching charactere
        for key, value in GAME.world.items():
            if not isinstance(value, Personn):
                continue
            if value.has_name(self.object):
                return value
        return None