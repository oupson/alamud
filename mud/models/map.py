# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .thing import Thing
from PIL import Image, ImageDraw
from io import BytesIO
import base64

class Map(Thing):

    """A special object that displays the location of the person looking at it"""

    #--------------------------------------------------------------------------
    # initialization
    #--------------------------------------------------------------------------
    
    def __init__(self, **kargs):
        super().__init__(**kargs)
        self.map_location = None
        self.locations = list()

    #--------------------------------------------------------------------------
    # initialization from YAML data
    #--------------------------------------------------------------------------

    def init_from_yaml(self, data, world):
        super().init_from_yaml(data, world)
        if data.get("map_location") is not None:
            self.map_location = data.get("map_location")
        if data.get("locations") is not None:
            self.locations = data.get("locations")

    def update_from_yaml(self, data, world):
        super().update_from_yaml(data, world)

    #--------------------------------------------------------------------------
    # API for saving the dynamic part of objects to YAML (via JSON)
    #--------------------------------------------------------------------------

    def archive_into(self, obj):
        super().archive_into(obj)

    def gen_map(self, actor):
        img = Image.open("./mud/games/ankoin_mystery/" + self.map_location) # TODO renommage
    
        location = actor.container()

        for l in self.locations:
            if location.id in l["id"]:
                draw = ImageDraw.Draw(img)
                x, y = l["coord_x"], l["coord_y"]
                draw.ellipse([x - 10, y-10, x + 10, y +10], "red")
                break

        buffer = BytesIO()
        img.save(buffer,format="JPEG")
        myimage = buffer.getvalue()     
        return base64.b64encode(myimage).decode("utf-8")