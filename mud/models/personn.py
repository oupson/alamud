from .thing import Thing
from .mixins.located import Located

class Personn(Thing):
    """"A non player character"""
    def __init__(self, **kargs):
        super().__init__(**kargs)
        self.questions = list()
        self.showables = dict()
        self.noun = "à"
        self.is_killer = False

    #--------------------------------------------------------------------------
    # initialization from YAML data
    #--------------------------------------------------------------------------

    def init_from_yaml(self, data, world):
        super().init_from_yaml(data, world)
        q = data.get("questions")
        if q is not None:
            self.questions = q
        
        s = data.get("showables")
        if s is not None:
            self.showables = {k:v for d in s for k, v in d.items()}
        n = data.get("noun")
        if n is not None:
            self.noun = n
        is_killer = data.get("is_killer")
        if is_killer is not None:
            self.is_killer = is_killer

    def update_from_yaml(self, data, world):
        super().update_from_yaml(data, world)

    #--------------------------------------------------------------------------
    # API for saving the dynamic part of objects to YAML (via JSON)
    #--------------------------------------------------------------------------

    def archive_into(self, obj):
        super().archive_into(obj)

    def get_noun(self):
        return "%s %s" % (self.noun, self.name)

    def noun_a(self):
        return self.name
